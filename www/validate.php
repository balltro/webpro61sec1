<?php

$comment = "<h1>Hey There! How are you doing today</h1>";
$sanitizedComment = filter_var($comment, FILTER_SANITIZE_STRING);
echo $comment;
echo $sanitizedComment;
echo "<br>";

///////////// Validate Integer
$int = 20;
if(filter_var($int, FILTER_VALIDATE_INT)){
	echo "The <b>$int</b> is a valid integer";
} else {
	echo "The <b>$int</b> is not a valid integer";
}

///////////// Validate Email
$email = "someone@example.com";
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
echo "<br>";
if(filter_var($email,FILTER_VALIDATE_EMAIL)){
	echo "Ther <b>$email</b> is valid email address";
}else{
	echo "Ther <b>$email</b> is not valid email address";
}

///////////// Validate URL
$url = "http://www.example.com";
$url = filter_var($url,FILTER_SANITIZE_URL);
echo "<br>";
if(filter_var($url, FILTER_VALIDATE_URL)){
	echo "The <b>$url</b> is a valid website url";
}else{
	echo "The <b>$url</b> is not a valid website url";
}

?>